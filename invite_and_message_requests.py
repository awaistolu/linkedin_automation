import requests
import json
import re
import uuid
import flask
from flask import Flask, request, jsonify
from flask_cors import CORS
from bs4 import BeautifulSoup


app = flask.Flask(__name__)
app.config["DEBUG"] = True

CORS(app)

cors = CORS(app, resource={
    r"/*": {
        "origins": "*"
    }
})



def cookie_data():
    LINKEDIN_BASE_URL = "https://www.linkedin.com"
    AUTH_REQUEST_HEADERS = {
        "X-Li-User-Agent": "LIAuthLibrary:3.2.4 com.linkedin.LinkedIn:8.8.1 iPhone:8.3",
        "User-Agent": "LinkedIn/8.8.1 CFNetwork/711.3.18 Darwin/14.0.0",
        "X-User-Language": "en",
        "X-User-Locale": "en_US",
        "Accept-Language": "en-us",
    }

    # Create a new session
    session = requests.Session()

    # Send a GET request to LinkedIn's authentication page
    response = session.get(f"{LINKEDIN_BASE_URL}/uas/authenticate", headers=AUTH_REQUEST_HEADERS)
    jsid = session.cookies['JSESSIONID']
    cookies = session.cookies
    bscookie = session.cookies['bscookie']
    return jsid, cookies,  bscookie

def authentication_request(username, password):
        """
        Authenticate with Linkedin.

        Return a session object that is authenticated.
        """

        LINKEDIN_BASE_URL = "https://www.linkedin.com"
        AUTH_REQUEST_HEADERS = {
            "X-Li-User-Agent": "LIAuthLibrary:3.2.4 com.linkedin.LinkedIn:8.8.1 iPhone:8.3",
            "User-Agent": "LinkedIn/8.8.1 CFNetwork/711.3.18 Darwin/14.0.0",
            "X-User-Language": "en",
            "X-User-Locale": "en_US",
            "Accept-Language": "en-us",
    }

        jsid, cookies, bscookie = cookie_data()
        payload = {
            "session_key": username,
            "session_password": password,
            "JSESSIONID": f"{jsid}",
        }

        res = requests.post(
            f"{LINKEDIN_BASE_URL}/uas/authenticate",
            data=payload,
            cookies=cookies,
            headers=AUTH_REQUEST_HEADERS,
        )
        cookie = res.cookies
        data = res.json()
        
        return data, cookie, bscookie


def cookies_headers(cookie, bscookie):
    
    cookies = {
    'li_rm': 'AQEQq9YnT7SEVwAAAYn5M-t_ErF3Wifh_ugOIYpaS-7xrC1TPqPQ7EF0KmYO632O7a57lNkdlgu8fm8m84jIifeDsXW9SezR7eZ2pOg5T5VqI42MT6V_kF7d',
    'bcookie': '"v=2&d9a64974-014e-4e92-8cf0-d2c4b507a57f"',
    'bscookie': f"{bscookie}",
    'li_theme': 'light',
    'li_theme_set': 'app',
    'timezone': 'Asia/Karachi',
    'li_sugr': 'c6fb7791-23ff-4136-8697-3577e2b64aa3',
    'lms_ads': 'AQFl1oU16x27VAAAAYpKJ9pH8_-W7_iUDFQwoTslmcuFGaIZmKvUqrvUnp2EK9V77qwYue9e4hSOmzo-erbmynWuVY0HJqdL',
    'lms_analytics': 'AQFl1oU16x27VAAAAYpKJ9pH8_-W7_iUDFQwoTslmcuFGaIZmKvUqrvUnp2EK9V77qwYue9e4hSOmzo-erbmynWuVY0HJqdL',
    'visit': 'v=1&M',
    'AnalyticsSyncHistory': 'AQL1p-__B8Am4gAAAYqJ7UNd-_KHJMlI9EfDW4UxYKBU2YSC6WlUZb7gayOpNApl8lOuJZhZKan9YrgLrBhg4g',
    'AMCV_14215E3D5995C57C0A495C55%40AdobeOrg': '-637568504%7CMCIDTS%7C19614%7CMCMID%7C80222749680920635193768637267016133022%7CMCAAMLH-1695187012%7C3%7CMCAAMB-1695187012%7C6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y%7CMCOPTOUT-1694589413s%7CNONE%7CvVersion%7C5.1.1',
    'aam_uuid': '80422938642732986443784093083100600917',
    'fid': 'AQE56I7rkVDrZgAAAYqOEUEAg5MnRaBGs4JcwvA_Kz2BsX96k1df6cX5dDsG03nJM2eMj0H2UX1pew',
    'g_state': '{"i_p":1695290831390,"i_l":3}',
    '__ssid': '70f4c015-9016-4ed7-ae82-c8ff752dfa6e',
    'dfpfpt': '3ea07f4dc92948a68b5f464d90a21bbf',
    'JSESSIONID': f"{cookie['JSESSIONID']}",
    'lang': 'v=2&lang=en-us',
    'liap': 'true',
    'li_at': f"{cookie['li_at']}",
    'lidc': '"b=VB36:s=V:r=V:a=V:p=V:g=10474:u=487:x=1:i=1695192707:t=1695268365:v=2:sig=AQEGr36Jxg1eh9pz3t77UBeid35rKk8P"',
    'UserMatchHistory': 'AQLSRgHCIFicpAAAAYqxXAc1Vymy-3N3aMopyLuMcPj5a7Bjya5EdbhfzYBLkg0gDavLohdOYAWb4n1D8UftX2_68XhNr53ld43V51Tzcl3nNaX0gHho9Uym2jtzYluxoR43UrjfOpPHgJlIHW4S7sZkERt3iQr7nx_qNg2uuO4A4P9eBl7z5-GeVIs5CYFRouetvN4rV9SoofKhmsS3b6v-NorAe7y00gdlSoZjJzfwbiDEaGxTmsqColxhhh0zICPNuYmwIcnLWJxVse6C2_us0QYD7dxuqEj7X4Fv7F3j5t62YY5EJu9Yb2reHzWeXdY6-64rOTroyRfySijhD18-LKRK9Uo',
}

    headers = {
        'authority': 'www.linkedin.com',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
        'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
        'cache-control': 'max-age=0',
        # 'cookie': 'li_rm=AQEQq9YnT7SEVwAAAYn5M-t_ErF3Wifh_ugOIYpaS-7xrC1TPqPQ7EF0KmYO632O7a57lNkdlgu8fm8m84jIifeDsXW9SezR7eZ2pOg5T5VqI42MT6V_kF7d; bcookie="v=2&d9a64974-014e-4e92-8cf0-d2c4b507a57f"; bscookie="v=1&20230815123756b9809cdb-d56a-44bf-80c2-78a11d258e10AQEt6rbWTqWjalK0oDjXW9xptDGVY8ij"; li_theme=light; li_theme_set=app; timezone=Asia/Karachi; li_sugr=c6fb7791-23ff-4136-8697-3577e2b64aa3; lms_ads=AQFl1oU16x27VAAAAYpKJ9pH8_-W7_iUDFQwoTslmcuFGaIZmKvUqrvUnp2EK9V77qwYue9e4hSOmzo-erbmynWuVY0HJqdL; lms_analytics=AQFl1oU16x27VAAAAYpKJ9pH8_-W7_iUDFQwoTslmcuFGaIZmKvUqrvUnp2EK9V77qwYue9e4hSOmzo-erbmynWuVY0HJqdL; visit=v=1&M; AnalyticsSyncHistory=AQL1p-__B8Am4gAAAYqJ7UNd-_KHJMlI9EfDW4UxYKBU2YSC6WlUZb7gayOpNApl8lOuJZhZKan9YrgLrBhg4g; AMCV_14215E3D5995C57C0A495C55%40AdobeOrg=-637568504%7CMCIDTS%7C19614%7CMCMID%7C80222749680920635193768637267016133022%7CMCAAMLH-1695187012%7C3%7CMCAAMB-1695187012%7C6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y%7CMCOPTOUT-1694589413s%7CNONE%7CvVersion%7C5.1.1; aam_uuid=80422938642732986443784093083100600917; fid=AQE56I7rkVDrZgAAAYqOEUEAg5MnRaBGs4JcwvA_Kz2BsX96k1df6cX5dDsG03nJM2eMj0H2UX1pew; g_state={"i_p":1695290831390,"i_l":3}; __ssid=70f4c015-9016-4ed7-ae82-c8ff752dfa6e; dfpfpt=3ea07f4dc92948a68b5f464d90a21bbf; JSESSIONID="ajax:4181620297794564086"; lang=v=2&lang=en-us; liap=true; li_at=AQEDATNER8QAVcEGAAABirFb8SMAAAGK1Wh1I04Ah1tMEqZ08xXGZeL_h5FotAEfayVm3yTkIMmeaMojw2m6VI9hrMJ56F0BmoVMiegL45ZhTo9CTzEakOR4pgGnwi9BJLhmlIDKnHLZfetaG-B8lxMX; lidc="b=VB36:s=V:r=V:a=V:p=V:g=10474:u=487:x=1:i=1695192707:t=1695268365:v=2:sig=AQEGr36Jxg1eh9pz3t77UBeid35rKk8P"; UserMatchHistory=AQLSRgHCIFicpAAAAYqxXAc1Vymy-3N3aMopyLuMcPj5a7Bjya5EdbhfzYBLkg0gDavLohdOYAWb4n1D8UftX2_68XhNr53ld43V51Tzcl3nNaX0gHho9Uym2jtzYluxoR43UrjfOpPHgJlIHW4S7sZkERt3iQr7nx_qNg2uuO4A4P9eBl7z5-GeVIs5CYFRouetvN4rV9SoofKhmsS3b6v-NorAe7y00gdlSoZjJzfwbiDEaGxTmsqColxhhh0zICPNuYmwIcnLWJxVse6C2_us0QYD7dxuqEj7X4Fv7F3j5t62YY5EJu9Yb2reHzWeXdY6-64rOTroyRfySijhD18-LKRK9Uo',
        'dnt': '1',
        'referer': 'https://www.linkedin.com/checkpoint/challenge/AgGGq4XRp7oOyQAAAYqxW6SjbwiXFy_ibsrATkviJUBCd0vDTrGNJ5CG-UKT_kNBAFvJLcjofmAAfdJ9tE6UN2E_qZMMrg?ut=33-3Du0SQr7aY1',
        'sec-ch-ua': '"Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Linux"',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-user': '?1',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
    }
    return cookies, headers


def get_urn_id(url, cookies, headers):

    profile_url = f"{url}"

    # Send a GET request to the LinkedIn profile page

    response = requests.get(profile_url, cookies=cookies, headers=headers)

    # Parse the HTML content of the page using BeautifulSoup

    soup = BeautifulSoup(response.content, "html.parser")

    # Find the script tag that contains the JSON data
    script_tag = soup.find_all('code')

    # Extract the JSON data as a string 
    json_dic = []
    # # Parse the JSON string into a Python dict
    for tag in script_tag:
        json_str = tag.text.strip()
        if "{" in json_str:
           json_dic.append(json_str)
    

    # Define a regular expression pattern to find urn:li:fsd_profile
    urn_id = ''
    pattern = r'urn:li:fsd_profile:([^,}]+)"'
    for data in json_dic:
        # Use re.findall to find all occurrences of the pattern in the string
        matches = re.findall(pattern, data)
        for match in matches:
            if "ACoAADNER8QB_-23JiUgv7m44efNIkrd30JpziU" not in match:
                # print(match)
                urn_id += match
                break


    return urn_id


def send_message(urn_id, profile_url, cookies):
      
  id = f"urn:li:fsd_profile:{urn_id}" 
  url = "https://www.linkedin.com/voyager/api/voyagerMessagingDashMessengerMessages?action=createMessage"

  csrf = cookies['JSESSIONID'].strip('"')

  payload = '{\"message\":{\"body\":{\"attributes\":[],\"text\":\"hello\"},\"originToken\":\"'+str(uuid.uuid4())+'\",\"renderContentUnions\":[]},\"mailboxUrn\":\"urn:li:fsd_profile:ACoAADNER8QB_-23JiUgv7m44efNIkrd30JpziU\",\"trackingId\":\"thmfTJ\\u000e.¾ý\\u000e´\\u0001\",\"dedupeByClientGeneratedToken\":false,\"hostRecipientUrns\":[\"'+str(id)+'\"]}'
  headers_1 = {
    'authority': 'www.linkedin.com',
    'accept': 'application/json',
    'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
    'content-type': 'text/plain;charset=UTF-8',
    'cookie': 'li_rm='+str(cookies['li_rm'])+'; bcookie='+str(cookies['bcookie'])+'; bscookie='+str(cookies['bscookie'])+'; li_theme=light; li_theme_set=app; timezone=Asia/Karachi; li_sugr='+str(cookies['li_sugr'])+'; lms_ads='+str(cookies['lms_ads'])+'; lms_analytics='+str(cookies['lms_analytics'])+'; visit=v=1&M; AnalyticsSyncHistory='+str(cookies['AnalyticsSyncHistory'])+'; aam_uuid='+str(cookies['aam_uuid'])+'; g_state={"i_p":1695290831390,"i_l":3}; __ssid='+str(cookies['__ssid'])+'; dfpfpt='+str(cookies['dfpfpt'])+'; JSESSIONID='+str(cookies['JSESSIONID'])+'; liap=true; li_at='+str(cookies['li_at'])+'; lang=v=2&lang=en-us; lidc="b=VB36:s=V:r=V:a=V:p=V:g=10474:u=487:x=1:i=1695273240:t=1695354805:v=2:sig=AQHBEchg7iZxS2hhd_BvbGf8BUqjV9Pa"; UserMatchHistory='+str(cookies['UserMatchHistory'])+'; bcookie='+str(cookies['bcookie'])+'',
    'csrf-token': str(csrf),
    'dnt': '1',
    'origin': 'https://www.linkedin.com',
    'referer': profile_url,
    'sec-ch-ua': '"Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Linux"',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-origin',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
    'x-li-lang': 'en_US',
    'x-li-track': '{"clientVersion":"1.13.3678","mpVersion":"1.13.3678","osName":"web","timezoneOffset":5,"timezone":"Asia/Karachi","deviceFormFactor":"DESKTOP","mpName":"voyager-web","displayDensity":1,"displayWidth":1920,"displayHeight":1080}',
    'x-restli-protocol-version': '2.0.0'
  }

  response = requests.request("POST", url, headers=headers_1, data=payload.encode())

  return json(response)


def invite_api(urn_id, cookies, headers):
    
    params = {
    'action': 'verifyQuotaAndCreateV2',
    'decorationId': 'com.linkedin.voyager.dash.deco.relationships.InvitationCreationResultWithInvitee-2',
    }

    json_data = {
        'invitee': {
            'inviteeUnion': {
                'memberProfile': f'urn:li:fsd_profile:{urn_id}',
            },
        },
    }

    response = requests.post(
        'https://www.linkedin.com/voyager/api/voyagerRelationshipsDashMemberRelationships',
        params=params,
        cookies=cookies,
        headers=headers,
        json=json_data,
    )

    return json(response)


def follow_api(urn_id, cookies, headers):
    
    json_data = {
    'patch': {
        '$set': {
            'following': True,
        },
    },
    }

    response = requests.post(
        f'https://www.linkedin.com/voyager/api/feed/dash/followingStates/urn:li:fsd_followingState:urn:li:fsd_profile:{urn_id}',
        cookies=cookies,
        headers=headers,
        json=json_data,
    )
    return json(response)


@app.route('/invite_and_message', methods=['POST'])
def main():
    data = request.get_json()
    profile_urls = data['urls'] # access url from JSON data
    email = data['email']
    password = data['pass']
    # message = data['message']
    send = data['send']
    data_1, cookie, bscookie = authentication_request(email, password)
    cookies, headers = cookies_headers(cookie, bscookie)
    for profile_url in profile_urls:
        urn_id = get_urn_id(profile_url, cookies, headers)
        for text in send:
            if 'message' in text:
                try:
                   send_api =  send_message(urn_id, profile_url, cookies)
                except:
                    continue
            elif 'invite' in text:
                try:
                   invitation = invite_api(urn_id, cookies, headers)
                except:
                    invitation = "Nan"
                
                if "Nan" in invitation:
                    try:
                      follow_person = follow_api(urn_id,cookies, headers)
                    except:
                        continue
            else:
                continue

    return jsonify({"Invite & Message": "Done"})

if __name__ == '__main__':
    app.run(port=8080)
