import flask
from flask import Flask, request, jsonify
from flask_cors import CORS
import undetected_chromedriver as uc
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import re
import threading
import numpy as np
import gspread
from linkedin_api import Linkedin
import concurrent.futures  # Import the concurrent.futures module

app = flask.Flask(__name__)
app.config["DEBUG"] = True

CORS(app)

cors = CORS(app, resource={
    r"/*": {
        "origins": "*"
    }
})

def new_driver():
    # Create a new undetected Chromedriver instance
    options = uc.ChromeOptions()

    # Set options to run Chromedriver in headless mode
    # options.add_argument('--headless')
    options.add_argument('--disable-gpu')

    # Set options to prevent Chrome from showing the "Chrome is being controlled by automated test software" notification
    options.add_argument('--disable-logging')
    options.add_argument('--disable-extensions')
    options.add_argument('--disable-infobars')
    options.add_argument('--disable-blink-features=AutomationControlled')
    options.add_argument("--disable-dev-shm-usage")
    options.add_argument("--no-sandbox")

    # Create a new undetected Chromedriver instance with the specified options
    driver = webdriver.Chrome(options=options)
    
    # Logging into LinkedIn
    driver.get("https://linkedin.com/login")
    time.sleep(5)

    username = driver.find_element(By.ID, "username")
    username.send_keys("ma579812@gmail.com")  # Enter Your Email Address

    pword = driver.find_element(By.ID, "password")
    pword.send_keys("A1!B2@C3#D4$")  # Enter Your Password

    driver.find_element(By.XPATH, "//button[@type='submit']").click()
    return driver

all_drivers = []

# Opening Multiple Drivers and Returning Them
def open_multiple_drivers(num_drivers):
    drivers = []
    for _ in range(num_drivers):
        driver = new_driver()
        all_drivers.append(driver)

@app.route('/driver_lists', methods=['POST'])
def driver_lists():
    number = int(request.form['number_of_driver'])
    open_multiple_drivers(number)
    return jsonify({"driver": "Login"})

def number_of_pages(num):
    if num <= 9:
        pages = 1
    else:
        pages = int(np.round(num / 10))
    return pages

def data_to_sheet(linkedin, worksheet, text):
    try:
       profile = linkedin.get_profile(f'{text}')
    except:
       profile = "Nan"
    # try:
    #    skills = linkedin.get_profile_skills(f'{text}')
    # except:
    #     skills = "Nan"
    # try:
    #    contact_info = linkedin.get_profile_contact_info(f'{text}')
    # except:
    #     contact_info = "Nan"

    name = profile['firstName'] + f" {profile['lastName']}"
    about = profile['summary']
    headline = profile['headline']
    location = profile['geoLocationName'] + f', {profile["geoCountryName"]}'
    profile_url = 'https://www.linkedin.com/in/' + profile['public_id']
    experience = profile['experience']
    certifications = profile['certifications']
    volunteer = profile['volunteer']

    experience_data = []

    for exp in experience:

        try:
            company_name = exp['companyName']
        except:
            company_name = 'Nan'
        try:
            job_title = exp['title']
        except:
            job_title = 'Nan'
        try:
            job_description = exp['description']
        except:
            job_description = 'Nan'
        try:
            time_period = exp['timePeriod']
        except:
            time_period = 'Nan'
        
        # Create a dictionary for the current experience
        exp_dict = {
            "Company Name": company_name,
            "Job Title": job_title,
            "Job Description": job_description,
            "Time Period": time_period
        }
        
        # Append the dictionary to the list
        experience_data.append(exp_dict)

    # Create an empty list to store dictionaries for each certification
    certification_data = []

    for cert in certifications:
        try:
            institute_name = cert['authority']
        except KeyError:
            institute_name = 'Nan'
        try:
            certification_name = cert['name']
        except KeyError:
            certification_name = 'Nan'
        try:
            time_period = cert['timePeriod']
        except KeyError:
            time_period = 'Nan'

        # Create a dictionary for the current certification
        cert_dict = {
            "Institute Name": institute_name,
            "Certification Name": certification_name,
            "Time Period": time_period
        }

        # Append the dictionary to the list
        certification_data.append(cert_dict)
    # Convert lists to strings
    # contact_info_str = "\n".join(contact_info)  # Join contact_info list with newlines
    experience_data_str = "\n".join(map(str, experience_data))  # Convert experience_data to a string
    certification_data_str = "\n".join(map(str, certification_data))  # Convert certification_data to a string
    # Extract skill names from the list of dictionaries
    # skills_list = [skill['name'] for skill in skills]
    # Extract relevant information from the volunteer data
    volunteer_info = []
    for item in volunteer:
        role = item.get('role', 'Nan')
        company_name = item.get('companyName', 'Nan')
        cause = item.get('cause', 'Nan')
        description = item.get('description', 'Nan')
        time_period = item.get('timePeriod', {})

        # Format the extracted information as a string
        volunteer_str = f"Role: {role}\nCompany Name: {company_name}\nCause: {cause}\nDescription: {description}\nTime Period: {time_period}"

        volunteer_info.append(volunteer_str)

    # Join the volunteer information into a single string with newline characters
    volunteer_str = "\n\n".join(volunteer_info)

    # Join the extracted skill names into a single string with newline characters
    # skills_str = "\n".join(skills_list)


    # Define the data to append
    data_to_append = [profile_url, name, about, headline, location, experience_data_str, certification_data_str, volunteer_str]
    print(data_to_append)
    # Append the data to the worksheet
    worksheet.append_row(data_to_append)

# Function to log in to Google Sheets 4 and retrieve a worksheet
def google_sheet_login():
    
    sa = gspread.service_account(filename="/home/tech/Music/linkedin-398310-bfa71d200543.json")
    sh = sa.open("linkedin.csv")

    wks = sh.worksheet("Sheet1")
    
    return wks

def chunk_list(number, n):
    nums_list = list(range(1, number + 1))
    chunks = [nums_list[i:i + n] for i in range(0, len(nums_list), n)]
    return chunks

linkedin = Linkedin('awaismushtaq4545@gmail.com', 'A1!B2@C3#D4$')

# Create a thread pool with a maximum of 4 threads (adjust as needed)
executor = concurrent.futures.ThreadPoolExecutor(max_workers=4)

# Modify the get_response function to work with a thread pool
def get_response(driver, search_url, linkedin, worksheet):
    # driver = new_driver()  # Create a new driver for each thread
    driver.get(search_url)
    time.sleep(10)

    all_links = driver.find_elements(By.XPATH, '//a[@class="app-aware-link "]')

    for link in all_links:
        profile_url = link.get_attribute("href")
        if "https://www.linkedin.com/in" in profile_url:
            regex = r"in/([^/?]+)"
            match = re.search(regex, profile_url)
            if match:
                text = match.group(1)
                # Check if the profile URL is already in the set
                # if profile_url not in result_set:
                data_to_sheet(linkedin, worksheet, text)
                    # profile = linkedin.get_profile(f'{text}')
                    # print(profile)
                    # result_set.add(profile_url)  # Add the profile URL to the set
                    # result_list.append(profile)  # Append the profile to the list
            else:
                print("No match found.")

@app.route('/profile_scrape', methods=['POST'])
def profile_scrape():
    url = request.form['url']
    pages_number = int(request.form['number_of_pages'])
    pages = number_of_pages(pages_number)
    chunks = chunk_list(pages, 4)
    all_profiles_data = []  # Create a list to hold profile data
    profile_url_set = set()  # Create a set to keep track of processed profile URLs
    worksheet = google_sheet_login()
    # Use a thread pool to concurrently execute the get_response function
    futures = []

    for chnk in chunks:
        for index, page_number in enumerate(chnk):
            driver = all_drivers[index] 
            search_url = f"{url}&page={page_number}"
            future = executor.submit(get_response, driver, search_url, linkedin, worksheet)
            futures.append(future)

    # Wait for all threads to complete
    concurrent.futures.wait(futures)

    return jsonify({"profiles_data": "complete data add in google sheet"})

if __name__ == '__main__':
    app.run()
