import flask
from flask import Flask, request, jsonify
from flask_cors import CORS
import undetected_chromedriver as uc
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

app = flask.Flask(__name__)
app.config["DEBUG"] = True

CORS(app)

cors = CORS(app, resource={
    r"/*": {
        "origins": "*"
    }
})


def new_driver():
    # Create a new undetected Chromedriver instance
    options = uc.ChromeOptions()

    # Set options to run Chromedriver in headless mode
    # options.add_argument('--headless')
    options.add_argument('--disable-gpu')

    # Set options to prevent Chrome from showing the "Chrome is being controlled by automated test software" notification
    options.add_argument('--disable-logging')
    options.add_argument('--disable-extensions')
    options.add_argument('--disable-infobars')
    options.add_argument('--disable-blink-features=AutomationControlled')
    options.add_argument("--disable-dev-shm-usage")
    options.add_argument("--no-sandbox")

    # Create a new undetected Chromedriver instance with the specified options
    driver = webdriver.Chrome(options=options)

    # Logging into LinkedIn
    driver.get("https://linkedin.com/login")

    # Wait for the login page to load
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "username")))

    username = driver.find_element(By.ID, "username")
    username.send_keys("ma579812@gmail.com")  # Enter Your Email Address

    pword = driver.find_element(By.ID, "password")
    pword.send_keys("A1!B2@C3#D4$")  # Enter Your Password

    driver.find_element(By.XPATH, "//button[@type='submit']").click()

    return driver


all_drivers = []

# Opening Multiple Drivers and Returning Them
def open_driver():
   
    driver = new_driver()
    all_drivers.append(driver)

# new_driver = new_driver()
@app.route('/driver', methods=['GET'])
def driver():
    open_driver()
    return jsonify({"driver": "Login"})

@app.route('/invite_and_message', methods=['POST'])
def invite_and_message():
    # data = request.get_json()
    data = request.get_json()

    urls = data['urls'] # access url from JSON data
    message = data['message']
    send = data['send']
    print(send)
    # invite_text = list(invite_text)
    new_driver = all_drivers[0]

    for url in urls:
        new_driver.get(url)
        time.sleep(3)
        for text in send:
            print(text)
            time.sleep(10)
            if 'invite' in text:
            
                try:
                    # Wait for the 'Message' button to become visible
                    connect_button = WebDriverWait(new_driver, 10).until(
                        EC.visibility_of_element_located((By.XPATH, "//button[@class='artdeco-button artdeco-button--2 artdeco-button--primary ember-view pvs-profile-actions__action']"))
                    )

                    # Get the text of the 'Message' button
                    connect_button_text = connect_button.text.strip()

                    if "Connect" in connect_button_text:
                        connect_button = WebDriverWait(new_driver, 10).until(
                        EC.visibility_of_element_located((By.XPATH, "//button[@class='artdeco-button artdeco-button--2 artdeco-button--primary ember-view pvs-profile-actions__action']"))
                    )

                    # Get the text of the 'Message' button
                        connect_button.click()

                        
                        send_button = WebDriverWait(new_driver, 10).until(
                        EC.visibility_of_element_located((By.XPATH, '//button[@aria-label="Send now"]'))
                    )

                    # Get the text of the 'Message' button
                        send_button.click()

                except:
                    print("connect button not located")


                try:
                    # Wait for the 'Message' button to become visible
                    connect_button = WebDriverWait(new_driver, 10).until(
                        EC.visibility_of_element_located((By.XPATH, "//button[@class='artdeco-button artdeco-button--2 artdeco-button--primary ember-view pvs-profile-actions__action']"))
                    )

                    # Get the text of the 'Message' button
                    connect_button_text = connect_button.text.strip()

                    if "Follow" in connect_button_text:
                        follow_button = WebDriverWait(new_driver, 10).until(
                        EC.visibility_of_element_located((By.XPATH, "//button[@class='artdeco-button artdeco-button--2 artdeco-button--primary ember-view pvs-profile-actions__action']"))
                    )

                    # Get the text of the 'Message' button
                        follow_button.click()

                except:
                    print("connect button not located")
            
            elif 'message' in text:

                try:
                    # Wait for the 'Message' button to become visible
                    message_button = WebDriverWait(new_driver, 2).until(
                        EC.visibility_of_element_located((By.XPATH, "//button[@class='artdeco-button artdeco-button--2 artdeco-button--secondary ember-view pvs-profile-actions__action']"))
                    )

                    # Get the text of the 'Message' button
                    message_button_text = message_button.text.strip()
                    print(message_button_text)
                    if "Message" in message_button_text:

                        # Wait for the 'Message' button to become visible
                        message_button = WebDriverWait(new_driver, 2).until(
                            EC.visibility_of_element_located((By.XPATH, "//button[@class='artdeco-button artdeco-button--2 artdeco-button--secondary ember-view pvs-profile-actions__action']"))
                        )

                        message_button.click()

                        # Wait for the message input field to become clickable
                        message_input = WebDriverWait(new_driver, 2).until(
                            EC.element_to_be_clickable((By.XPATH, '//div[@aria-label="Write a message…"]'))
                        )

                        message_input.click()
                        message_input.send_keys(message)

                        # Send the message
                        send_button = WebDriverWait(new_driver, 2).until(
                            EC.element_to_be_clickable((By.XPATH, ' //button[@class="msg-form__send-button artdeco-button artdeco-button--1"]'))
                        )

                        send_button.click()

                except:
                    print("Not send messege")

                try:
                    # Wait for the 'Message' button to become visible
                    message_button = WebDriverWait(new_driver, 2).until(
                        EC.visibility_of_element_located((By.XPATH, "//button[@class='artdeco-button artdeco-button--2 artdeco-button--secondary ember-view pvs-profile-actions__action']"))
                    )

                    # Get the text of the 'Message' button
                    message_button_text = message_button[1].text.strip()
                    print(message_button_text)
                    if "Message" in message_button_text:

                        # Wait for the 'Message' button to become visible
                        message_button = WebDriverWait(new_driver, 2).until(
                            EC.visibility_of_element_located((By.XPATH, "//button[@class='artdeco-button artdeco-button--2 artdeco-button--secondary ember-view pvs-profile-actions__action']"))
                        )

                        message_button[1].click()

                        # Wait for the message input field to become clickable
                        message_input = WebDriverWait(new_driver, 2).until(
                            EC.element_to_be_clickable((By.XPATH, '//div[@aria-label="Write a message…"]'))
                        )

                        message_input.click()
                        message_input.send_keys(message)

                        # Send the message
                        send_button = WebDriverWait(new_driver, 2).until(
                            EC.element_to_be_clickable((By.XPATH, ' //button[@class="msg-form__send-button artdeco-button artdeco-button--1"]'))
                        )

                        send_button.click()

                except:
                    print("Not send messege")

                try:
                    # Wait for the 'Message' button to become visible
                    message_button = WebDriverWait(new_driver, 2).until(
                        EC.visibility_of_element_located((By.XPATH, "//button[@class='artdeco-button artdeco-button--2 artdeco-button--primary ember-view pvs-profile-actions__action']"))
                    )

                    # Get the text of the 'Message' button
                    message_button_text = message_button.text.strip()
                    print(message_button_text)
                    if "Message" in message_button_text:

                        # Wait for the 'Message' button to become visible
                        message_button = WebDriverWait(new_driver, 2).until(
                            EC.visibility_of_element_located((By.XPATH, "//button[@class='artdeco-button artdeco-button--2 artdeco-button--primary ember-view pvs-profile-actions__action']"))
                        )

                        message_button.click()

                        # Wait for the message input field to become clickable
                        message_input = WebDriverWait(new_driver, 2).until(
                            EC.element_to_be_clickable((By.XPATH, '//div[@aria-label="Write a message…"]'))
                        )

                        message_input.click()
                        message_input.send_keys(message)

                        # Send the message
                        send_button = WebDriverWait(new_driver, 2).until(
                            EC.element_to_be_clickable((By.XPATH, ' //button[@class="msg-form__send-button artdeco-button artdeco-button--1"]'))
                        )

                        send_button.click()

                except:
                    print("Not send messege")
            
            
            else:
                print("Error")

    return jsonify("Done")

if __name__ == '__main__':
    app.run(port=8080)
