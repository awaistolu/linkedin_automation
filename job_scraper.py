import requests
from bs4 import BeautifulSoup
import concurrent.futures
import re
from flask import Flask, jsonify, request
from flask_cors import CORS

app = Flask(__name__)
app.config["DEBUG"] = True

CORS(app)

cors = CORS(app, resource={
    r"/*": {
        "origins": "*"
    }
})

# Function to scrape job URLs from a given URL


def cookie_data():
    LINKEDIN_BASE_URL = "https://www.linkedin.com"
    AUTH_REQUEST_HEADERS = {
        "X-Li-User-Agent": "LIAuthLibrary:3.2.4 com.linkedin.LinkedIn:8.8.1 iPhone:8.3",
        "User-Agent": "LinkedIn/8.8.1 CFNetwork/711.3.18 Darwin/14.0.0",
        "X-User-Language": "en",
        "X-User-Locale": "en_US",
        "Accept-Language": "en-us",
    }

    # Create a new session
    session = requests.Session()

    # Send a GET request to LinkedIn's authentication page
    response = session.get(f"{LINKEDIN_BASE_URL}/uas/authenticate", headers=AUTH_REQUEST_HEADERS)
    jsid = session.cookies['JSESSIONID']
    cookies = session.cookies
    bscookie = session.cookies['bscookie']
    return jsid, cookies,  bscookie



def scrape_job_urls(url, headers):
    url_list = []
    
    response = requests.request("GET", url, headers=headers)
    soup = BeautifulSoup(response.text, "html.parser")
    all_div = soup.find_all("a", {"data-tracking-control-name": "public_jobs_jserp-result_search-card"})
    
    for i in all_div:
        url_list.append(i.get("href"))
        # print(i.get("href"))
    
    return url_list

def scrape_url(url):

    num = 25
    url_list = []
    cookie = cookie_data()
    csrf =  cookie[0]
    # print(csrf)
    # Define the regular expression pattern to extract data from "search/" onward
    pattern = r'search/.*'

    # Use the re.search() function to find the match
    match = re.search(pattern, url)

    if match:
        # Extract the matched portion
        extracted_data = match.group(0)
        # print("Extracted Data:", extracted_data)
    else:
        print("No data matching the pattern found in the URL.")

    base_url = "https://www.linkedin.com/jobs-guest/jobs/api/seeMoreJobPostings/" + extracted_data + "&start={}"
    
    # Set the number of concurrent workers
    num_workers = 5  # You can adjust this number based on your system's capacity

    # Define the headers here
    headers = {
    'authority': 'www.linkedin.com',
    'accept': '*/*',
    'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
    'cookie': 'li_rm=AQEQq9YnT7SEVwAAAYn5M-t_ErF3Wifh_ugOIYpaS-7xrC1TPqPQ7EF0KmYO632O7a57lNkdlgu8fm8m84jIifeDsXW9SezR7eZ2pOg5T5VqI42MT6V_kF7d; bcookie="v=2&d9a64974-014e-4e92-8cf0-d2c4b507a57f"; bscookie="v=1&20230815123756b9809cdb-d56a-44bf-80c2-78a11d258e10AQEt6rbWTqWjalK0oDjXW9xptDGVY8ij"; li_theme=light; li_theme_set=app; timezone=Asia/Karachi; li_sugr=c6fb7791-23ff-4136-8697-3577e2b64aa3; AnalyticsSyncHistory=AQKqrWpJbHfdSgAAAYpKJ9fgi8EpdtJGAuqIkiK0Ll1tlcc-PbzxaxwLC7mV-Mf5DNF1r0ahtY3cAMCBUfAeDg; lms_ads=AQFl1oU16x27VAAAAYpKJ9pH8_-W7_iUDFQwoTslmcuFGaIZmKvUqrvUnp2EK9V77qwYue9e4hSOmzo-erbmynWuVY0HJqdL; lms_analytics=AQFl1oU16x27VAAAAYpKJ9pH8_-W7_iUDFQwoTslmcuFGaIZmKvUqrvUnp2EK9V77qwYue9e4hSOmzo-erbmynWuVY0HJqdL; visit=v=1&M; sdsc=22%3A1%2C1694502345330%7EJAPP%2C0QPu4lc15bTm29B19pX7qh4brTJg%3D; UserMatchHistory=AQI3Y2kI1RXlSAAAAYqIOPkFpO8Wlb30TBqd1bOKmsQyouK367gRLNplsD5w7Jak8VYAoSzoMgltkA8-yVU7fimOh0MTg9OB6DWeK9GkSL2RZKhIZB9NfhL-YGENVjRfb0qMnqLiMBsOmXch6ZcquRUGxNrVEy_0nXXVBceAkv979hinFhffD9fihHlvTXvE8ZYxfvhzaqYrWGxRy7LlnEQI2WcaP2FD7EPd6qfCBqp5ydxaqvskLKEsIbwTEd9RW_I2PI6OY2WxPJ3kwx_L9SD_TW3WIXfsW7PHhz1exX2H2BIj5vJdKspk8X6ZmfIT5xdgBjIzBXMX76R_4z1A6oseDC-RHaA; li_g_recent_logout=v=1&true; lang=v=2&lang=en-us; lidc="b=OGST03:s=O:r=O:a=O:p=O:g=2993:u=1:x=1:i=1694502590:t=1694588990:v=2:sig=AQHJIFd2SKub_3-R6p2bs9_VbFFBqjfN"; JSESSIONID="ajax:8185586498311892777"; g_state={"i_p":1694589223317,"i_l":2}; recent_history=AQF8sQ8FExnrlAAAAYqIPTNm1hC-BWKhupKjStmEGEbnvrZb9xnx5LQd-ygBlyKrf952ZYf2KAD9piRw1oXZaJKsJaPk9jX6W7uwfcLDp7iwz6bCmt4wT39RWm6JtMCniUB0G_oxlKL2684xFotohbvotd6AkyX6OxFa6CRY3sqOLPns6npqw-5nGddF6tPFA45pQx6MKAyYmfUBe36ZCGPqjAvt-zm4jX9U-31phPlx1yXkd5_TJfZrB5Gd8kwMK-sGJzOaSAOQOh9k5TU4dmM2WeJUBmC24B8l8s6Z5SnuUtVnpKG_IdmMz_RanqqlTM9EOBRhPuH84n1-rWg1FQ3FhutKo2Pi9vi-MdJ9SvoqBw; bcookie="v=2&d9a64974-014e-4e92-8cf0-d2c4b507a57f"; recent_history=AQGFDSoiIoS4rQAAAYqIQ7SsAc9vEhzXXv17IMtjk8TxMI65Jo7cHCmlOx_Bc6iY9I66suXs392TfWjRSgXszf-MO8kzoCfimbKt52JjvyI7mLoNIhpJ2i5GewO-LhOHHnci9ZZOUJ0IG96sgjZgvzFEYtHXVFxMuGw2mlTSXvNS-Uozn_LQFdCsyVN60jZJlQyLIlcdQTHVWKV8c-pekzE8b8bXQleeBCJqU79EP9q-CAU7LEsQpW5Xv6KpBJmtb4iOL4TNg1aVG2H6vs3k8WrfY6ol_20AHmf1a2_La6ivTzCSlWqOqRzKhwz5WdwDzSicUzs51KjFvGB3QHRa3FvPfJba2H9fsvuqEAp3oiqYytM',
    'csrf-token': str(csrf),
    'dnt': '1',
    'referer': base_url,
    'sec-ch-ua': '"Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Linux"',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-origin',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'
    }

    with concurrent.futures.ThreadPoolExecutor(max_workers=num_workers) as executor:
        # Create a list of URLs with different 'start' values
        urls = [base_url.format(num + i * 25) for i in range(11)]
        
        # Fetch job URLs concurrently
        results = executor.map(scrape_job_urls, urls, [headers] * len(urls))
        
        # Combine the results into a single list
        for result in results:
            url_list.extend(result)

    return url_list

# Now 'url_list' contains all the scraped job URLs


# Function to scrape job data from a given URL
def scrape_job_data(url):
    cookie = cookie_data()
    headers = {
    'authority': 'www.linkedin.com',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
    'accept-language': 'en-GB,en;q=0.9',
    'cache-control': 'max-age=0',
    # 'cookie': 'fid='+str(cookie['fid'])+'; JSESSIONID='+str(cookie['JSESSIONID'])+'; lang=v=2&lang=en-us; bcookie='+str(cookie['bcookie'])+'; bscookie='+str(cookie['bscookie'])+'; fcookie='+str(cookie['fcookie'])+'; _gcl_au=1.1.1881324304.1695706112; AMCVS_14215E3D5995C57C0A495C55%40AdobeOrg=1; AMCV_14215E3D5995C57C0A495C55%40AdobeOrg=-637568504%7CMCIDTS%7C19627%7CvVersion%7C5.1.1%7CMCMID%7C38593689286711511680380548030470885003%7CMCAAMLH-1696310912%7C3%7CMCAAMB-1696310912%7C6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y%7CMCOPTOUT-1695713312s%7CNONE; aam_uuid='+str(cookie['aam_uuid'])+'; recent_history='+str(cookie['recent_history'])+'; lidc="b=TGST03:s=T:r=T:a=T:p=T:g=3050:u=1:x=1:i=1695707444:t=1695793844:v=2:sig=AQH1es49IKfyOB2osZkhzRL5xqK-wPai"; _uetsid=87c72dc05c2d11eeba977dbb0e964c41; _uetvid=87c798305c2d11ee8228ffa7deb37a73',
    'dnt': '1',
    'sec-ch-ua': '"Google Chrome";v="117", "Not;A=Brand";v="8", "Chromium";v="117"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Linux"',
    'sec-fetch-dest': 'document',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-site': 'none',
    'sec-fetch-user': '?1',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
    }
    response = requests.request("GET", url, headers=headers)
    soup = BeautifulSoup(response.text, "html.parser")
    dic = {}  # Define the 'dic' variable before the try block
    
    try:
        title = soup.find('h1').text.strip()
        description = soup.find('div', {'class': 'description__text description__text--rich'}).text
        main_des = soup.find('ul', {'class': "description__job-criteria-list"})
        li_tag = main_des.find_all('li')
        
        try:
            Seniority_level = li_tag[0].find('span').text.strip()
        except:
            Seniority_level = "Nan"
        
        try:
            Employment_type = li_tag[1].find('span').text.strip()
        except:
            Employment_type = "Nan"
        
        try:
            Job_function = li_tag[2].find('span').text.strip()
        except:
            Job_function = "Nan"
        
        try:
            Industries = li_tag[3].find('span').text.strip()
        except:
            Industries = "Nan"

        dic = {'title': title, "description": description, "level": Seniority_level, "type": Employment_type,
            "job_function": Job_function, "industries": Industries}
    except:
        pass
    
    return dic

@app.route("/get_jobs", methods=["GET"])
def job_scrape():
    jobs_data = []
    url = request.form['url']
    url_list = scrape_url(url)
    # Set the number of concurrent workers
    num_workers = 7  # You can adjust this number based on your system's capacity

    with concurrent.futures.ThreadPoolExecutor(max_workers=num_workers) as executor:
        # Fetch job data concurrently
        results = executor.map(scrape_job_data, url_list)
        
        # Iterate over the results and append to the jobs_data list
        for result in results:
            if result:  # Check if 'result' is not empty before appending
                print(result)
                jobs_data.append(result)

    return jsonify({"jobs_data":jobs_data})

# Now 'jobs_data' contains all the scraped job data


if __name__ == '__main__':
    app.run()