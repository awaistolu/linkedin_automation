import flask
from flask import Flask, request, jsonify
from flask_cors import CORS
from bs4 import BeautifulSoup
import requests
import re
import numpy as np
import time
import random
from linkedin_api import Linkedin
import threading
import concurrent.futures  # Import the concurrent.futures module

app = flask.Flask(__name__)
app.config["DEBUG"] = True

CORS(app)

cors = CORS(app, resource={
    r"/*": {
        "origins": "*"
    }
})
linkedin = Linkedin('awaismushtaq4545@gmail.com', 'A1!B2@C3#D4$')

# Define a lock to synchronize access to shared data
data_lock = threading.Lock()

# Define a list to store the scraped profiles
scraped_profiles = []

def scrape_profile(name):
    try:
        profile = linkedin.get_profile(name)
        # print(profile)
        with data_lock:
            scraped_profiles.append(profile)
        # return profile
        
    except:
        pass



def cookie_data():
    LINKEDIN_BASE_URL = "https://www.linkedin.com"
    AUTH_REQUEST_HEADERS = {
        "X-Li-User-Agent": "LIAuthLibrary:3.2.4 com.linkedin.LinkedIn:8.8.1 iPhone:8.3",
        "User-Agent": "LinkedIn/8.8.1 CFNetwork/711.3.18 Darwin/14.0.0",
        "X-User-Language": "en",
        "X-User-Locale": "en_US",
        "Accept-Language": "en-us",
    }

    # Create a new session
    session = requests.Session()

    # Send a GET request to LinkedIn's authentication page
    response = session.get(f"{LINKEDIN_BASE_URL}/uas/authenticate", headers=AUTH_REQUEST_HEADERS)
    jsid = session.cookies['JSESSIONID']
    cookies = session.cookies
    bscookie = session.cookies['bscookie']
    return jsid, cookies,  bscookie

def authentication_request(username, password):
        """
        Authenticate with Linkedin.

        Return a session object that is authenticated.
        """

        LINKEDIN_BASE_URL = "https://www.linkedin.com"
        AUTH_REQUEST_HEADERS = {
            "X-Li-User-Agent": "LIAuthLibrary:3.2.4 com.linkedin.LinkedIn:8.8.1 iPhone:8.3",
            "User-Agent": "LinkedIn/8.8.1 CFNetwork/711.3.18 Darwin/14.0.0",
            "X-User-Language": "en",
            "X-User-Locale": "en_US",
            "Accept-Language": "en-us",
    }

        jsid, cookies, bscookie = cookie_data()
        payload = {
            "session_key": username,
            "session_password": password,
            "JSESSIONID": f"{jsid}",
        }

        res = requests.post(
            f"{LINKEDIN_BASE_URL}/uas/authenticate",
            data=payload,
            cookies=cookies,
            headers=AUTH_REQUEST_HEADERS,
        )
        cookie = res.cookies
        data = res.json()
        
        return data, cookie, bscookie

def number_of_pages(num):
    if num <= 9:
        pages = 1
    else:
        pages = int(np.round(num / 10))
    return pages

def profile_urls(cookie, bscookie, url, page_number):

 

        url = f"{url}&page={page_number}"

        # Define a regex pattern to match the keyword parameter in the URL
        pattern = r'keywords=([^&]+)'
        name = ''
        # Use re.search to find the match
        match = re.search(pattern, url)

        # Extract the keyword from the match
        if match:
            keyword = match.group(1)
            # print(keyword)
            name += keyword
        else:
            print("Keyword not found in the URL.")


        cookies = {
            'li_rm': 'AQEQq9YnT7SEVwAAAYn5M-t_ErF3Wifh_ugOIYpaS-7xrC1TPqPQ7EF0KmYO632O7a57lNkdlgu8fm8m84jIifeDsXW9SezR7eZ2pOg5T5VqI42MT6V_kF7d',
            'bcookie': '"v=2&d9a64974-014e-4e92-8cf0-d2c4b507a57f"',
            'bscookie': f"{bscookie}",
            'li_theme': 'light',
            'li_theme_set': 'app',
            'timezone': 'Asia/Karachi',
            'li_sugr': 'c6fb7791-23ff-4136-8697-3577e2b64aa3',
            'lms_ads': 'AQFl1oU16x27VAAAAYpKJ9pH8_-W7_iUDFQwoTslmcuFGaIZmKvUqrvUnp2EK9V77qwYue9e4hSOmzo-erbmynWuVY0HJqdL',
            'lms_analytics': 'AQFl1oU16x27VAAAAYpKJ9pH8_-W7_iUDFQwoTslmcuFGaIZmKvUqrvUnp2EK9V77qwYue9e4hSOmzo-erbmynWuVY0HJqdL',
            'visit': 'v=1&M',
            'AnalyticsSyncHistory': 'AQL1p-__B8Am4gAAAYqJ7UNd-_KHJMlI9EfDW4UxYKBU2YSC6WlUZb7gayOpNApl8lOuJZhZKan9YrgLrBhg4g',
            'AMCV_14215E3D5995C57C0A495C55%40AdobeOrg': '-637568504%7CMCIDTS%7C19614%7CMCMID%7C80222749680920635193768637267016133022%7CMCAAMLH-1695187012%7C3%7CMCAAMB-1695187012%7C6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y%7CMCOPTOUT-1694589413s%7CNONE%7CvVersion%7C5.1.1',
            'aam_uuid': '80422938642732986443784093083100600917',
            'fid': 'AQE56I7rkVDrZgAAAYqOEUEAg5MnRaBGs4JcwvA_Kz2BsX96k1df6cX5dDsG03nJM2eMj0H2UX1pew',
            'g_state': '{"i_p":1695290831390,"i_l":3}',
            '__ssid': '70f4c015-9016-4ed7-ae82-c8ff752dfa6e',
            'dfpfpt': '3ea07f4dc92948a68b5f464d90a21bbf',
            'fptctx2': 'taBcrIH61PuCVH7eNCyH0GhRK1qyu05Z2j7BNvR3fMXhZIZkJ6k950qvXKGRICye4O7y8DFJ%252f5c0idR05l04oECxU9ZEp%252fDl1bbaw0I675bK49z%252fyH5ClwTXKc8RrWVSDPGsKTOmOfjMnNXuqxJSAeljJD8LoIyMV7amMafL8JhM2JNjtf5JQlDHrotvqG%252bzYs9lRLatBfhx2s9oWAAtAesb4wXYLWLgNdeWSnYdZuLy7tnzL6tm5x%252bvk%252fLbj6L%252ffTLIFUmZaJ4dIQ9RTimd7UvUugdRxcLtMDY0v3BaocyizMupd9b6JqFNivrv%252fSYVBEJN6jPPo6s5lmGJCd%252baAwUHPxrtpleSl4pF%252fVxb17U%253d',
            'PLAY_LANG': 'en',
            'PLAY_SESSION': 'eyJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7InNlc3Npb25faWQiOiIyZDlkYzkzNi02NDE0LTRlN2ItYjg4MS04NzAxMzhhODZkOTF8MTY5NDY5MzI4MiIsImFsbG93bGlzdCI6Int9IiwicmVjZW50bHktc2VhcmNoZWQiOiIiLCJyZWZlcnJhbC11cmwiOiJodHRwczovL3d3dy5saW5rZWRpbi5jb20vaGVscC9saW5rZWRpbi9hbnN3ZXIvYTUyMjkzNSIsInJlY2VudGx5LXZpZXdlZCI6IiIsIkNQVC1pZCI6InU0XHUwMDE2w4lHXHUwMDBGwrxcYnhcdTAwMUVjXHUwMDAye1x1MDAxOUDCvCIsImZsb3dUcmFja2luZ0lkIjoiMS93OXg0S1pUclNDajI2MUlZelI2Zz09IiwiZXhwZXJpZW5jZSI6IiIsInRyayI6IiJ9LCJuYmYiOjE2OTQ3MDE3NjEsImlhdCI6MTY5NDcwMTc2MX0.RDk4T678OP3QSedaSvXadnhTRr6yjr4AOVj6lWUTKYk',
            'li_g_recent_logout': 'v=1&true',
            'lang': 'v=2&lang=en-us',
            'JSESSIONID': f"{cookie['JSESSIONID']}",
            'sdsc': '22%3A1%2C1695022245692%7EJAPP%2C05wWLO8XANirDU7myIol1cuBBuqc%3D',
            'liap': 'true',
            'li_at': f"{cookie['li_at']}",
            'lidc': '"b=OB00:s=O:r=O:a=O:p=O:g=7642:u=11:x=1:i=1695022297:t=1695107142:v=2:sig=AQEezzwIUyZP4y0jH6oui_B6fEguAA6x"',
            'UserMatchHistory': 'AQLNBVhkXBJWhwAAAYqnM9bxBbmP7Ff27BK8Ed7ia-KLC-zLmMFghIBK3UCC_gyRK4rx5IqFy-bBe0as-IgTShpcx54DL6Ca-pXMMe2LbH4mEPFFju5TNF2yJq-WLymj-NvL0E53csPJuvIPglLDWPgw9MseKS3VV7UwCAHwLgcdKmAoZR7Md7h-BfIzQdM9DWD482kxyyHNALb_F4unaUBbroq33iqNJImNT5uzuFd1DrNlpld3Pp8h2ASj60IEY7fSlc2QThuT-xvoeB_3De_N6uUYIWVmXkmUKprcbYOk0sS7Dr9rWp5ZOwFdrIr-xmyV-9CP1HAgpHRrR5FJl1WlsIsXaC4',
        }

        headers = {
            'authority': 'www.linkedin.com',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
            'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
            'cache-control': 'max-age=0',
            # 'cookie': 'li_rm=AQEQq9YnT7SEVwAAAYn5M-t_ErF3Wifh_ugOIYpaS-7xrC1TPqPQ7EF0KmYO632O7a57lNkdlgu8fm8m84jIifeDsXW9SezR7eZ2pOg5T5VqI42MT6V_kF7d; bcookie="v=2&d9a64974-014e-4e92-8cf0-d2c4b507a57f"; bscookie="v=1&20230815123756b9809cdb-d56a-44bf-80c2-78a11d258e10AQEt6rbWTqWjalK0oDjXW9xptDGVY8ij"; li_theme=light; li_theme_set=app; timezone=Asia/Karachi; li_sugr=c6fb7791-23ff-4136-8697-3577e2b64aa3; lms_ads=AQFl1oU16x27VAAAAYpKJ9pH8_-W7_iUDFQwoTslmcuFGaIZmKvUqrvUnp2EK9V77qwYue9e4hSOmzo-erbmynWuVY0HJqdL; lms_analytics=AQFl1oU16x27VAAAAYpKJ9pH8_-W7_iUDFQwoTslmcuFGaIZmKvUqrvUnp2EK9V77qwYue9e4hSOmzo-erbmynWuVY0HJqdL; visit=v=1&M; AnalyticsSyncHistory=AQL1p-__B8Am4gAAAYqJ7UNd-_KHJMlI9EfDW4UxYKBU2YSC6WlUZb7gayOpNApl8lOuJZhZKan9YrgLrBhg4g; AMCV_14215E3D5995C57C0A495C55%40AdobeOrg=-637568504%7CMCIDTS%7C19614%7CMCMID%7C80222749680920635193768637267016133022%7CMCAAMLH-1695187012%7C3%7CMCAAMB-1695187012%7C6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y%7CMCOPTOUT-1694589413s%7CNONE%7CvVersion%7C5.1.1; aam_uuid=80422938642732986443784093083100600917; fid=AQE56I7rkVDrZgAAAYqOEUEAg5MnRaBGs4JcwvA_Kz2BsX96k1df6cX5dDsG03nJM2eMj0H2UX1pew; g_state={"i_p":1695290831390,"i_l":3}; __ssid=70f4c015-9016-4ed7-ae82-c8ff752dfa6e; dfpfpt=3ea07f4dc92948a68b5f464d90a21bbf; fptctx2=taBcrIH61PuCVH7eNCyH0GhRK1qyu05Z2j7BNvR3fMXhZIZkJ6k950qvXKGRICye4O7y8DFJ%252f5c0idR05l04oECxU9ZEp%252fDl1bbaw0I675bK49z%252fyH5ClwTXKc8RrWVSDPGsKTOmOfjMnNXuqxJSAeljJD8LoIyMV7amMafL8JhM2JNjtf5JQlDHrotvqG%252bzYs9lRLatBfhx2s9oWAAtAesb4wXYLWLgNdeWSnYdZuLy7tnzL6tm5x%252bvk%252fLbj6L%252ffTLIFUmZaJ4dIQ9RTimd7UvUugdRxcLtMDY0v3BaocyizMupd9b6JqFNivrv%252fSYVBEJN6jPPo6s5lmGJCd%252baAwUHPxrtpleSl4pF%252fVxb17U%253d; PLAY_LANG=en; PLAY_SESSION=eyJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7InNlc3Npb25faWQiOiIyZDlkYzkzNi02NDE0LTRlN2ItYjg4MS04NzAxMzhhODZkOTF8MTY5NDY5MzI4MiIsImFsbG93bGlzdCI6Int9IiwicmVjZW50bHktc2VhcmNoZWQiOiIiLCJyZWZlcnJhbC11cmwiOiJodHRwczovL3d3dy5saW5rZWRpbi5jb20vaGVscC9saW5rZWRpbi9hbnN3ZXIvYTUyMjkzNSIsInJlY2VudGx5LXZpZXdlZCI6IiIsIkNQVC1pZCI6InU0XHUwMDE2w4lHXHUwMDBGwrxcYnhcdTAwMUVjXHUwMDAye1x1MDAxOUDCvCIsImZsb3dUcmFja2luZ0lkIjoiMS93OXg0S1pUclNDajI2MUlZelI2Zz09IiwiZXhwZXJpZW5jZSI6IiIsInRyayI6IiJ9LCJuYmYiOjE2OTQ3MDE3NjEsImlhdCI6MTY5NDcwMTc2MX0.RDk4T678OP3QSedaSvXadnhTRr6yjr4AOVj6lWUTKYk; li_g_recent_logout=v=1&true; lang=v=2&lang=en-us; JSESSIONID="ajax:4181620297794564086"; sdsc=22%3A1%2C1695022245692%7EJAPP%2C05wWLO8XANirDU7myIol1cuBBuqc%3D; liap=true; li_at=AQEDAUAZmEAE5IBgAAABiqczqSMAAAGKy0AtI1YAnNa4PoEEEj_FrmF2wEcXA_C7wrhKgCtePeySN0p_uVED4AuIfgaFfzqNdDA1lqFDdgxV-pPYLOWGBrSXrlnZn43oMttRR9y_ogVoYKNdhjgGaldB; lidc="b=OB00:s=O:r=O:a=O:p=O:g=7642:u=11:x=1:i=1695022297:t=1695107142:v=2:sig=AQEezzwIUyZP4y0jH6oui_B6fEguAA6x"; UserMatchHistory=AQLNBVhkXBJWhwAAAYqnM9bxBbmP7Ff27BK8Ed7ia-KLC-zLmMFghIBK3UCC_gyRK4rx5IqFy-bBe0as-IgTShpcx54DL6Ca-pXMMe2LbH4mEPFFju5TNF2yJq-WLymj-NvL0E53csPJuvIPglLDWPgw9MseKS3VV7UwCAHwLgcdKmAoZR7Md7h-BfIzQdM9DWD482kxyyHNALb_F4unaUBbroq33iqNJImNT5uzuFd1DrNlpld3Pp8h2ASj60IEY7fSlc2QThuT-xvoeB_3De_N6uUYIWVmXkmUKprcbYOk0sS7Dr9rWp5ZOwFdrIr-xmyV-9CP1HAgpHRrR5FJl1WlsIsXaC4',
            'dnt': '1',
            'referer': 'https://www.linkedin.com/',
            'sec-ch-ua': '"Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"',
            'sec-ch-ua-mobile': '?0',
            'sec-ch-ua-platform': '"Linux"',
            'sec-fetch-dest': 'document',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-user': '?1',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
        }

        profile_url = f"{url}&page={page_number}"
        time.sleep(random.uniform(0.6,1.7))
        response = requests.get(profile_url, headers=headers, cookies=cookies, allow_redirects=False)
        #   output_json = html_to_json.convert(response.text)


        pattern = r'https://www.linkedin.com/in/.*?\?'

        matches = re.findall(pattern, response.text)
        profiles = []

        ignore_url = "https://www.linkedin.com/talent/job-management-redirect?"

        for match in matches:
            if ignore_url in match:
                continue  # Skip this URL
            pattern = r"https://www.linkedin.com/in/.*?{}.*?\?".format(name)
            qasim_matches = re.findall(pattern, match)
            
            for qasim_match in qasim_matches:
                if qasim_match not in profiles:
                    profiles.append(qasim_match)


        # Define a regular expression pattern to extract the desired part of the URL
        pattern = r'\/in\/(.*?)\?'

        # Initialize a list to store the extracted parts
        extracted_names = []

        # Loop through the LinkedIn URLs and extract the parts
        for linkedin_url in profiles:
            match = re.search(pattern, linkedin_url)
            if match:
                extracted_part = match.group(1)
                extracted_names.append(extracted_part)

        # Now extracted_parts will contain the extracted parts of the LinkedIn profile URLs

        return extracted_names


@app.route('/profile_scrape', methods=['POST'])
def main():
    url = request.form['url']
    num_of_profiles = int(request.form['number_of_pages'])
    # url = "https://www.linkedin.com/search/results/people/?keywords=awais&origin=SWITCH_SEARCH_VERTICAL&sid=yO%2C"
    num_pages = number_of_pages(num_of_profiles)
    print(num_pages)
    futures = []
    profiles_data = []
    data, cookie, bscookie = authentication_request('ma579812@gmail.com', 'A1!B2@C3#D4$')
    print(data)
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        # Fetch job data concurrently
        for page_number in range(1, num_pages + 1):
            profile_urls_list = profile_urls(cookie, bscookie, url, page_number)
            for profile_name in profile_urls_list:
              future = executor.submit(scrape_profile, profile_name)
    
              futures.append(future)

        # Wait for all threads to complete
        concurrent.futures.wait(futures)

    # Print the scraped profiles
    for profile in scraped_profiles:
        if profile not in profiles_data:
            profiles_data.append(profile)
    
    scraped_profiles.clear()

    return jsonify({"profiles_data": profiles_data})

if __name__ == '__main__':
    app.run(port=8080)
